using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SaveSettings : MonoBehaviour
{
    public Button Save, Cancel, Defaults;
    // if > 0 when pressing the up button it will speed the forward movement
    public GameObject VerticalSpeed;
    // rotation speed of the spaceship
    public GameObject HorizontalSpeed;
    public GameObject SpaceShipSpeed;
    public GameObject SpaceShipLateralUnforcedSpeed;
    //public GameObject SpaceShipFowardUnforcedSpeed;
    public GameObject InsideCLoopBurstSpeed;
    public GameObject RatioMultipler;
    public GameObject WinHeight;

    public GameObject GravityForce;
    public GameObject FrictionForce;
    public GameObject EnergyLevel;
    public GameObject MinDecreaseEnergyStep;
    public GameObject MaxDecreaseEnergyStep;
    public GameObject FireBallDecreaseEnergy;

    // Start is called before the first frame update
    void Start()
    {
        // Load 
        Save.onClick.AddListener(SaveClick);
        Cancel.onClick.AddListener(CancelClick);
        Defaults.onClick.AddListener(DefaultsClick);
        VerticalSpeed.GetComponentInParent<TMPro.TMP_InputField>().text = PlayerPrefs.GetFloat("VerticalSpeed", 100).ToString();
        HorizontalSpeed.GetComponentInParent<TMPro.TMP_InputField>().text = PlayerPrefs.GetFloat("HorizontalSpeed", 100).ToString();
        SpaceShipSpeed.GetComponentInParent<TMPro.TMP_InputField>().text = PlayerPrefs.GetFloat("SpaceShipSpeed", 25).ToString();
        SpaceShipLateralUnforcedSpeed.GetComponentInParent<TMPro.TMP_InputField>().text = PlayerPrefs.GetFloat("SpaceShipLateralUnforcedSpeed", 15).ToString();
        InsideCLoopBurstSpeed.GetComponentInParent<TMPro.TMP_InputField>().text = PlayerPrefs.GetFloat("InsideCLoopBurstSpeed", 700).ToString();
        RatioMultipler.GetComponentInParent<TMPro.TMP_InputField>().text = PlayerPrefs.GetFloat("RatioMultipler", 1000).ToString();
        WinHeight.GetComponentInParent<TMPro.TMP_InputField>().text = PlayerPrefs.GetFloat("WinHeight", 350).ToString();
        GravityForce.GetComponentInParent<TMPro.TMP_InputField>().text = PlayerPrefs.GetFloat("GravityForce", 500).ToString();
        FrictionForce.GetComponentInParent<TMPro.TMP_InputField>().text = PlayerPrefs.GetFloat("FrictionForce", 150).ToString();
        EnergyLevel.GetComponentInParent<TMPro.TMP_InputField>().text = PlayerPrefs.GetFloat("EnergyLevel", 0.5f).ToString();
        MinDecreaseEnergyStep.GetComponentInParent<TMPro.TMP_InputField>().text = PlayerPrefs.GetFloat("MinDecreaseEnergyStep", .0001f).ToString();
        MaxDecreaseEnergyStep.GetComponentInParent<TMPro.TMP_InputField>().text = PlayerPrefs.GetFloat("MaxDecreaseEnergyStep", .055f).ToString();
        FireBallDecreaseEnergy.GetComponentInParent<TMPro.TMP_InputField>().text = PlayerPrefs.GetFloat("FireBallDecreaseEnergy", 1000).ToString();
    }

    private void Awake()
    {
        VerticalSpeed.GetComponentInParent<TMPro.TMP_InputField>().text = PlayerPrefs.GetFloat("VerticalSpeed", 100).ToString();
        HorizontalSpeed.GetComponentInParent<TMPro.TMP_InputField>().text = PlayerPrefs.GetFloat("HorizontalSpeed", 100).ToString();
        SpaceShipSpeed.GetComponentInParent<TMPro.TMP_InputField>().text = PlayerPrefs.GetFloat("SpaceShipSpeed", 25).ToString();
        SpaceShipLateralUnforcedSpeed.GetComponentInParent<TMPro.TMP_InputField>().text = PlayerPrefs.GetFloat("SpaceShipLateralUnforcedSpeed", 15).ToString();
        InsideCLoopBurstSpeed.GetComponentInParent<TMPro.TMP_InputField>().text = PlayerPrefs.GetFloat("InsideCLoopBurstSpeed", 700).ToString();
        RatioMultipler.GetComponentInParent<TMPro.TMP_InputField>().text = PlayerPrefs.GetFloat("RatioMultipler", 1000).ToString();
        WinHeight.GetComponentInParent<TMPro.TMP_InputField>().text = PlayerPrefs.GetFloat("WinHeight", 350).ToString();
        GravityForce.GetComponentInParent<TMPro.TMP_InputField>().text = PlayerPrefs.GetFloat("GravityForce", 500).ToString();
        FrictionForce.GetComponentInParent<TMPro.TMP_InputField>().text = PlayerPrefs.GetFloat("FrictionForce", 150).ToString();
        EnergyLevel.GetComponentInParent<TMPro.TMP_InputField>().text = PlayerPrefs.GetFloat("EnergyLevel", 0.5f).ToString();
        MinDecreaseEnergyStep.GetComponentInParent<TMPro.TMP_InputField>().text = PlayerPrefs.GetFloat("MinDecreaseEnergyStep", .0001f).ToString();
        MaxDecreaseEnergyStep.GetComponentInParent<TMPro.TMP_InputField>().text = PlayerPrefs.GetFloat("MaxDecreaseEnergyStep", .055f).ToString();
        FireBallDecreaseEnergy.GetComponentInParent<TMPro.TMP_InputField>().text = PlayerPrefs.GetFloat("FireBallDecreaseEnergy", 1000).ToString();
    }

    void SaveClick()
    {
        float value;
        if (float.TryParse(VerticalSpeed.GetComponentInParent<TMPro.TMP_InputField>().text, out value))
            PlayerPrefs.SetFloat("VerticalSpeed", value);
        if (float.TryParse(HorizontalSpeed.GetComponentInParent<TMPro.TMP_InputField>().text, out value))
            PlayerPrefs.SetFloat("HorizontalSpeed", value);
        if (float.TryParse(SpaceShipSpeed.GetComponentInParent<TMPro.TMP_InputField>().text, out value))
            PlayerPrefs.SetFloat("SpaceShipSpeed", value);
        if (float.TryParse(SpaceShipLateralUnforcedSpeed.GetComponentInParent<TMPro.TMP_InputField>().text, out value))
            PlayerPrefs.SetFloat("SpaceShipLateralUnforcedSpeed", value);
        if (float.TryParse(InsideCLoopBurstSpeed.GetComponentInParent<TMPro.TMP_InputField>().text, out value))
            PlayerPrefs.SetFloat("InsideCLoopBurstSpeed", value);
        if (float.TryParse(RatioMultipler.GetComponentInParent<TMPro.TMP_InputField>().text, out value))
            PlayerPrefs.SetFloat("RatioMultipler", value);
        if (float.TryParse(WinHeight.GetComponentInParent<TMPro.TMP_InputField>().text, out value))
            PlayerPrefs.SetFloat("WinHeight", value);
        if (float.TryParse(GravityForce.GetComponentInParent<TMPro.TMP_InputField>().text, out value))
            PlayerPrefs.SetFloat("GravityForce", value);
        if (float.TryParse(FrictionForce.GetComponentInParent<TMPro.TMP_InputField>().text, out value))
            PlayerPrefs.SetFloat("FrictionForce", value);
        if (float.TryParse(EnergyLevel.GetComponentInParent<TMPro.TMP_InputField>().text, out value))
            PlayerPrefs.SetFloat("EnergyLevel", value);
        if (float.TryParse(MinDecreaseEnergyStep.GetComponentInParent<TMPro.TMP_InputField>().text, out value))
            PlayerPrefs.SetFloat("MinDecreaseEnergyStep", value);
        if (float.TryParse(MaxDecreaseEnergyStep.GetComponentInParent<TMPro.TMP_InputField>().text, out value))
            PlayerPrefs.SetFloat("MaxDecreaseEnergyStep", value);
        if (float.TryParse(FireBallDecreaseEnergy.GetComponentInParent<TMPro.TMP_InputField>().text, out value))
            PlayerPrefs.SetFloat("FireBallDecreaseEnergy", value);
        PlayerPrefs.Save();
        SceneManager.LoadScene("Intro");
    }

    void CancelClick()
    {
        SceneManager.LoadScene("Intro");
    }

    void DefaultsClick()
    {
        VerticalSpeed.GetComponentInParent<TMPro.TMP_InputField>().text = "100";
        PlayerPrefs.SetFloat("VerticalSpeed", 100);
        HorizontalSpeed.GetComponentInParent<TMPro.TMP_InputField>().text = "100";
        PlayerPrefs.SetFloat("HorizontalSpeed", 100);
        SpaceShipSpeed.GetComponentInParent<TMPro.TMP_InputField>().text = "25";
        PlayerPrefs.SetFloat("SpaceShipSpeed", 25);
        SpaceShipLateralUnforcedSpeed.GetComponentInParent<TMPro.TMP_InputField>().text = "15";
        PlayerPrefs.SetFloat("SpaceShipLateralUnforcedSpeed", 15);
        InsideCLoopBurstSpeed.GetComponentInParent<TMPro.TMP_InputField>().text = "700";
        PlayerPrefs.SetFloat("InsideCLoopBurstSpeed", 700);
        RatioMultipler.GetComponentInParent<TMPro.TMP_InputField>().text = "1000";
        PlayerPrefs.SetFloat("RatioMultipler", 1000);
        WinHeight.GetComponentInParent<TMPro.TMP_InputField>().text = "350";
        PlayerPrefs.SetFloat("WinHeight", 350);
        GravityForce.GetComponentInParent<TMPro.TMP_InputField>().text = "500";
        PlayerPrefs.SetFloat("GravityForce", 500);
        FrictionForce.GetComponentInParent<TMPro.TMP_InputField>().text = "150";
        PlayerPrefs.SetFloat("FrictionForce", 150);
        EnergyLevel.GetComponentInParent<TMPro.TMP_InputField>().text = "0,5";
        PlayerPrefs.SetFloat("EnergyLevel", 0.5f);
        MinDecreaseEnergyStep.GetComponentInParent<TMPro.TMP_InputField>().text = "0,0001";
        PlayerPrefs.SetFloat("MinDecreaseEnergyStep", .0001f);
        MaxDecreaseEnergyStep.GetComponentInParent<TMPro.TMP_InputField>().text = "0,055";
        PlayerPrefs.SetFloat("MaxDecreaseEnergyStep", .055f);
        FireBallDecreaseEnergy.GetComponentInParent<TMPro.TMP_InputField>().text = "1000";
        PlayerPrefs.SetFloat("FireBallDecreaseEnergy", 1000);
    }
}
