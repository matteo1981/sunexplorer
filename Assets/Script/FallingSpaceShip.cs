using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingSpaceShip : MonoBehaviour
{
    public Vector3 Gravity;
    private Vector3 Velocity;

    void Update()
    {
        Velocity += Gravity * Time.deltaTime;   // allow gravity to work on our velocity vector
        transform.position += Velocity * Time.deltaTime;    // move us this frame according to our speed
    }
}
