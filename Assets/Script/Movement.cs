﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Movement : MonoBehaviour
{
    public const float DistanzaRiferimentoSpazio = 1.879157f;
    public const float ReduceVel = 5000;
    //default per terra
    [SerializeField]
    public float PeriodoRivoluzione = 1;
    [SerializeField]
    public float PeriodoRotazione = 1;
    [SerializeField]
    public bool OnlySelfRotation = false;
    [SerializeField]
    public bool RandomInitialPosition = true;

    Quaternion rotation;
    float currentRotation = 0.0f;
    float deltaRotation = 0.0f;
    Vector3 radius = new Vector3(5, 0, 0);
    [SerializeField]
    public GameObject TargetObj = null;
    public bool FallingObj = false;
    public float startRadiusFallingObj;
    public float endRadiusFallingObj;
    public float deltaFalling = .01f;

    private float currentRadius;
    private float deltaRadius;

    // Use this for initialization
    void Start()
    {
        radius = new Vector3(transform.position.z - TargetObj.transform.position.z, 0, 0);
        if (PeriodoRivoluzione > 0)
            deltaRotation = ((2 * Mathf.PI * radius.x) / PeriodoRivoluzione) / ReduceVel;
        else
            deltaRotation = 0;

        if (RandomInitialPosition && !OnlySelfRotation && !name.Equals("Moon") && TargetObj == GameObject.Find("Sole"))
        {
            currentRotation = (Random.value * 10000000000000000000) * deltaRotation;
            transform.RotateAround(TargetObj.transform.position, Vector3.up, currentRotation);
        }
        currentRadius = startRadiusFallingObj;
        deltaRadius = (startRadiusFallingObj - endRadiusFallingObj) * deltaFalling;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetKeyUp(KeyCode.Return))
        {
            SceneManager.LoadScene("SunPlay");
        }
        if (FallingObj)
        {
            // start - end : 100 = x : percent
            if (!(currentRadius == endRadiusFallingObj))
            {
                currentRadius = currentRadius - deltaRadius;
                transform.position = currentRadius * Vector3.Normalize(this.transform.position - TargetObj.transform.position) + TargetObj.transform.position;
            }
        }
        Rotate();
    }

    void Rotate()
    {
        if (!OnlySelfRotation)
        {
            currentRotation = Time.deltaTime * deltaRotation;
            transform.RotateAround(TargetObj.transform.position, Vector3.up, currentRotation);
        }
        if (PeriodoRotazione != 0)
        {
            float VelRotazione = (Time.deltaTime * ((2 * Mathf.PI) / PeriodoRotazione));
            transform.Rotate(0, VelRotazione, 0);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Sole")
        {
            SceneManager.LoadScene("SunPlay");
        }
    }
}
