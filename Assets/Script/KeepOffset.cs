using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class KeepOffset : MonoBehaviour
{
    public GameObject Target;
    public bool FreezeX, FreezeY, FreezeZ;
    private Vector3 offset;

    //public float SpeedMoveTo = 0f;
    public bool isCamera = false;
    //private bool restoreOffset = false;
    //public float delta = .1f;

    public float smoothTime = 0.25f;
    Vector3 currentVelocity;

    // Start is called before the first frame update
    void Start()
    {
        if (Target == null)
            return;

        offset = transform.position - Target.transform.position;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (Target == null)
            return;

        //if (isCamera)
        //{
        //    Vector3 viewPos = Camera.main.WorldToViewportPoint(Target.transform.position);
        //    if (!(viewPos.x >= 0 && viewPos.x <= 1 && viewPos.y >= 0 && viewPos.y <= 1 && viewPos.z > 0))
        //        // not visible
        //        restoreOffset = true;

        //    if (restoreOffset)
        //    {
        //        var step = SpeedMoveTo * Time.deltaTime; // calculate distance to move
        //        transform.position = Vector3.MoveTowards(transform.position, Target.transform.position + offset, step);
        //    }
        //    else
        //        SetPosition();

        //    if (Vector3.Distance(transform.position - Target.transform.position, offset) < delta)
        //        restoreOffset = false;

        //    return;
        //}

        SetPosition();
    }

    private void SetPosition()
    {
        if (isCamera)
        {
            transform.position = Vector3.SmoothDamp(
                transform.position,
                Target.transform.position + offset,
                ref currentVelocity,
                smoothTime
            );
        }
        else
        {
            Vector3 newPosition = Target.transform.position + offset;
            if (FreezeX)
            {
                newPosition.x = transform.position.x;
            }
            if (FreezeY)
            {
                newPosition.y = transform.position.y;
            }
            if (FreezeZ)
            {
                newPosition.z = transform.position.z;
            }
            transform.position = newPosition;
        }
    }
}
