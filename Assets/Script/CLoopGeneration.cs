using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.VFX;
using UnityEngine.XR;

public class CLoopGeneration : FireballGeneration
{    
    public static float CLoopLocalScaleMin = 20f, CLoopLocalScaleMax = 80;
    // from 0 to 1
    public float PercentRotateAhead = .6f;
    public float MinPlayRate = 30, MaxPlayRate = 500;

    // Use this for initialization
    void Start()
    {
        lGameObject = new List<GameObject>();
        Player = GameObject.FindGameObjectWithTag("Player");
        _PlayerLastPosition = Player.transform.position;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (InstantiateObject == null)
            return;

        if (this.GetNotDestroyedObjList() >= MaxParticle)
            return;

        for (int i = 0; i < SimultaneousObjects; i++)
            CreateObject();

        _PlayerLastPosition = Player.transform.position;
    }

    private int GetNotDestroyedObjList()
    {
        int res = 0;
        foreach (GameObject go in lGameObject)
            if (!go.IsDestroyed())
                res++;
        return res;
    }

    protected new void CreateObject()
    {
        GameObject obj;
        Vector3 hitPoint = GetBelowSpherePoint(Scale); //GetVisiblePosition(Scale, DistanceToPlayer);
        obj = Instantiate(InstantiateObject, hitPoint, Quaternion.identity);
        Vector3 direction = (Player.transform.position - _PlayerLastPosition);
        obj.transform.RotateAround(new Vector3(0, 0, 0), new Vector3(1, 0, Random.Range(1, -1)), (direction.z > 0 ? 1 : -1) * Random.Range(RangeRotMin, RangeRotMax));
        obj.name = "CLoop" + (Random.value * 1000).ToString("000");
        obj.transform.eulerAngles = new Vector3(
            obj.transform.eulerAngles.x,
            Random.Range(40, 160),
            obj.transform.eulerAngles.z
        );
        if (Random.value > PercentRotateAhead)
        {
            obj.transform.eulerAngles = new Vector3(
                obj.transform.eulerAngles.x,
                Random.Range(0, 360),
                obj.transform.eulerAngles.z
            );
        }

        float localScaleY = Mathf.Clamp((float)Random.value * (CLoopLocalScaleMax - CLoopLocalScaleMin) + CLoopLocalScaleMin, CLoopLocalScaleMin, CLoopLocalScaleMax);
        float localScaleX = Mathf.Clamp((float)Random.value * (CLoopLocalScaleMax - CLoopLocalScaleMin) + CLoopLocalScaleMin, CLoopLocalScaleMin, CLoopLocalScaleMax);
        obj.transform.localScale = new Vector3(
            localScaleX,
            localScaleY,
            obj.transform.localScale.z
            );
        float loopForce = (localScaleY - CLoopGeneration.CLoopLocalScaleMin) / (CLoopGeneration.CLoopLocalScaleMax - CLoopGeneration.CLoopLocalScaleMin);
        if (obj.transform.eulerAngles.y > 180)
            loopForce *= -1;

        GameObject text = GameObject.Find(obj.name + "/Canvas/Value");
        TextMeshProUGUI textmesh = text.GetComponent<TextMeshProUGUI>();
        if (textmesh != null)
        {
            float ratio = (localScaleY - CLoopGeneration.CLoopLocalScaleMin) / (CLoopGeneration.CLoopLocalScaleMax - CLoopGeneration.CLoopLocalScaleMin);
            if (obj.transform.eulerAngles.y > 180)
                ratio *= -1;
            //Debug.Log("Ratio: " + ratio);
            //float requestGravity = ratio * 2;
            //Debug.Log("requestGravity: " + requestGravity);
            if (ratio > 0)
            {
                textmesh.faceColor = new Color32(0, 255, 0, 255);
                textmesh.text = "+" + ratio.ToString("0.00");
            }
            else
            {
                textmesh.faceColor = new Color32(255, 0, 0, 255);
                textmesh.text = ratio.ToString("0.00");
            }
        }
        GameObject canvas = GameObject.Find(obj.name + "/Canvas");
        canvas.GetComponent<LookAtCamera>().Camera = Camera.main;

        VisualEffect ve = obj.GetComponentInChildren<VisualEffect>();
        ve.playRate = Random.value;

        Destroy(obj, DestroyTime);

        lGameObject.Add(obj);

    }

    public static float GetCoronalLoopEnergy(GameObject coronalLoop)
    {
        float energy = (coronalLoop.transform.localScale.y - CLoopGeneration.CLoopLocalScaleMin) / (CLoopGeneration.CLoopLocalScaleMax - CLoopGeneration.CLoopLocalScaleMin);
        if (coronalLoop.transform.eulerAngles.y > 180)
            energy *= -1;
        return energy;
    }


}

