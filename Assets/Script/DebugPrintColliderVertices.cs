﻿using System.Collections;
using UnityEngine;
public class DebugPrintColliderVertices : MonoBehaviour
{
    const uint NUM_VERTICES = 8;

    private BoxCollider boxCollider;
    private Transform[] vertices;

    void Awake()
    {
        boxCollider = (BoxCollider)this.gameObject.GetComponent(typeof(BoxCollider));

        if (boxCollider == null)
        {
            Debug.Log("Collider not found on " + this.gameObject.name + ". Ending script.");
            this.enabled = false;
        }

        vertices = new Transform[NUM_VERTICES];
    }

    void Update()
    {
        Vector3 colliderCentre = boxCollider.center;
        Vector3 colliderExtents = boxCollider.size;

        for (int i = 0; i != NUM_VERTICES; ++i)
        {
            Vector3 extents = colliderExtents;

            extents.Scale(new Vector3((i & 1) == 0 ? 1 : -1, (i & 2) == 0 ? 1 : -1, (i & 4) == 0 ? 1 : -1));

            Vector3 vertexPosLocal = colliderCentre + extents;

            Vector3 vertexPosGlobal = boxCollider.transform.TransformPoint(vertexPosLocal);

            // display vector3 to six decimal places
            Debug.Log("Vertex " + i + " @ " + vertexPosGlobal.ToString("F6"));
        }
    }
}