using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.VFX;

public class FireballGeneration : MonoBehaviour
{
    public GameObject InstantiateObject;
    public float Scale;
    public int SimultaneousObjects;
    public int MaxParticle;
    protected List<GameObject> lGameObject;
    public float DestroyTime = 15;
    public bool UseRayCast = true;
    public float MinMass, MaxMass;
    protected Vector3 _PlayerLastPosition;
    protected GameObject Player;
    public float RangeRotMin = 20, RangeRotMax = 50;

    // Use this for initialization
    void Start()
    {
        lGameObject = new List<GameObject>();
        Player = GameObject.FindGameObjectWithTag("Player");
        _PlayerLastPosition = Player.transform.position;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (InstantiateObject == null)
            return;

        if (this.GetNotDestroyedObjList() >= MaxParticle)
            return;

        for (int i = 0; i < SimultaneousObjects; i++)
            CreateObject();

        _PlayerLastPosition = Player.transform.position;
    }

    private int GetNotDestroyedObjList()
    {
        int res = 0;
        foreach (GameObject go in lGameObject)
            if (!go.IsDestroyed())
                res++;
        return res;
    }

    protected void CreateObject()
    {
        GameObject obj;
        Vector3 position = UseRayCast ? GetBelowSpherePoint(Scale) : Random.onUnitSphere * Scale;
        obj = Instantiate(InstantiateObject, position, Quaternion.identity);
        Rigidbody rb = obj.GetComponent<Rigidbody>();
        if (rb != null)
            rb.mass = Random.Range(MinMass, MaxMass);
        Vector3 direction = (Player.transform.position - _PlayerLastPosition);
        obj.transform.RotateAround(new Vector3(0, 0, 0), new Vector3(1, 0, Random.Range(1, -1)), (direction.z >= 0 ? 1 : -1) * Random.Range(RangeRotMin, RangeRotMax));
        obj.name = "Fireball" + (Random.value * 1000).ToString("000"); ;
        Destroy(obj, DestroyTime);
        lGameObject.Add(obj);
    }

    public static Vector3 GetBelowSpherePoint(float sphereScale)
    {
        GameObject SunPlane = GameObject.FindGameObjectWithTag("SunPlane");
        GameObject Player = GameObject.FindGameObjectWithTag("Player");
        Vector3 winner = Random.onUnitSphere * sphereScale;
        bool found = false;
        float minDistance = float.MaxValue;
        RaycastHit[] hits = Physics.RaycastAll(Player.transform.position, -(Player.transform.position - SunPlane.transform.position));
        foreach (var hit in hits)
        {
            if (hit.collider == null || hit.collider.gameObject == null)
                continue;
            if (hit.collider.gameObject.name == SunPlane.gameObject.name)
            {
                if (hit.distance < minDistance)
                {
                    winner = hit.point;
                    minDistance = hit.distance;
                    found = true;
                }
            }
        }
        if (!found)
            Debug.Log("Not Found!");
        return winner;
    }

    public static Vector3 GetVisiblePosition(float sphereScale, float distanceToPlayer, int tries = 1000000000)
    {
        GameObject SunPlane = GameObject.FindGameObjectWithTag("SunPlane");
        GameObject Player = GameObject.FindGameObjectWithTag("Player");
        Vector3 position = Random.onUnitSphere * sphereScale;
        for (int i = 0; i < tries; i++)
        {
            SphereCollider sc = SunPlane.GetComponent<SphereCollider>();
            float distanceStar = Vector3.Distance(sc.ClosestPoint(position), position);
            if (distanceStar <= 0)
                break;
            else
                position = Random.onUnitSphere * sphereScale;
        }
        for (int i = 0; i < tries; i++)
        {
            float distToPlayer = Vector3.Distance(position, Player.transform.position);
            if (distToPlayer < distanceToPlayer)
                return position;
            else
                position = Random.onUnitSphere * sphereScale;
        }
        return position;
    }
}
