using System.Collections.Generic;
using System.Drawing;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.VFX;

public class GameController : MonoBehaviour
{
    //public GameObject EngineFire;
    public GameObject Explosion;
    public GameObject Smoke;

    // if > 0 when pressing the up button it will speed the forward movement
    public float VerticalSpeed = 10.0f;
    // rotation speed of the spaceship
    public float HorizontalSpeed = 100.0f;
    public float SpaceShipSpeed = 10.0f;
    public float SpaceShipLateralUnforcedSpeed = 10.0f;
    public float SpaceShipFowardUnforcedSpeed = 15.0f;
    public float InsideLoopBurstSpeed = 100f;
    public float InsideLoopSmoothTime = 0.25f;
    public float InsideLoopDivFactor = 10;

    public float RatioMultipler = 5;

    // destroy time for explosion
    public float DestroyTime = 15f;
    private bool gameover = false;
    public GameObject GameOverObject;

    public GameObject SpaceshipExplosion;
    public float WinHeight;

    private Rigidbody rb;
    public float GravityForce;
    public float FrictionForce;
    public float _EnergyLevel = .4f;
    public Vector2 DecreaseEnergyStep = new Vector2(0.001f, 0.01f);
    public float FireBallDecreaseEnergy = 1000;
    private float _SpeedPlayer = 0f;

    public GameObject InfoText;
    public GameObject LoopText;
    public GameObject HelpText;
    public float F1HelpInfoSecondsToDisplay = 3f;
    private float started = 0;
    private GameObject HelpBackground;
    public GameObject EnergyLevelBar;
    private float _AppliedForce;
    private Vector3 prevPosition;
    private GameObject InsideLoop = null;
    private Vector3 currentVelocity;
    private int InsideLoopIndexCorner = -1;
    public float DeltaDistance = 3;

    public AudioSource ExplosionAudioSource;
    public AudioSource LoadingAudioSource;
    public AudioSource AlertAudioSouce;

    private const string InfoColor = "<#80ff80>"; //green
    private const string Info2Color = "<#008BFF>"; //blue
    private const string DangerColor = "<#DB0B00>"; //red
    private const string WarningColor = "<#FFF300>"; //yellow


    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        HelpBackground = GameObject.FindGameObjectWithTag("HelpBackground");
        started = Time.time;
        VerticalSpeed = PlayerPrefs.GetFloat("VerticalSpeed", 100);
        HorizontalSpeed = PlayerPrefs.GetFloat("HorizontalSpeed", 100);
        SpaceShipSpeed = PlayerPrefs.GetFloat("SpaceShipSpeed", 25);
        SpaceShipLateralUnforcedSpeed = PlayerPrefs.GetFloat("SpaceShipLateralUnforcedSpeed", 15);
        InsideLoopBurstSpeed = PlayerPrefs.GetFloat("InsideCLoopBurstSpeed", 700);
        RatioMultipler = PlayerPrefs.GetFloat("RatioMultipler", 1000);
        WinHeight = PlayerPrefs.GetFloat("WinHeight", 350);
        GravityForce = PlayerPrefs.GetFloat("GravityForce", 500);
        FrictionForce = PlayerPrefs.GetFloat("FrictionForce", 150);
        _EnergyLevel = PlayerPrefs.GetFloat("EnergyLevel", 0.5f);
        DecreaseEnergyStep.x =  PlayerPrefs.GetFloat("MinDecreaseEnergyStep", .0001f);
        DecreaseEnergyStep.y = PlayerPrefs.GetFloat("MaxDecreaseEnergyStep", .055f);
        FireBallDecreaseEnergy = PlayerPrefs.GetFloat("FireBallDecreaseEnergy", 1000);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetKeyUp(KeyCode.F1))
        {
            SceneManager.LoadScene("Intro");
        }

        if ((Time.time - started) > F1HelpInfoSecondsToDisplay)
        {
            HelpText.GetComponent<TMPro.TextMeshProUGUI>().text = "";
            HelpBackground.SetActive(false);
        }

        if (gameover)
        {
            if (Input.GetKeyUp(KeyCode.Return))
                SceneManager.LoadScene("SunPlay");

            CameraController ctrl = Camera.main.GetComponent<CameraController>();
            ctrl.ElevationAngle = 50;
            ctrl.FollowDistance = 50;
            return;
        }

        GameObject SunPlane = GameObject.FindGameObjectWithTag("SunPlane");
        SphereCollider sc = SunPlane.GetComponent<SphereCollider>();
        sc.ClosestPointOnBounds(transform.position);
        float distanceStar = Vector3.Distance(sc.ClosestPoint(transform.position), transform.position);
        if (distanceStar > WinHeight)
        {
            SceneManager.LoadScene("YouWin");
            return;
        }
        if (prevPosition != null)
        {
            Vector3 currVel = (transform.position - prevPosition) / Time.deltaTime;
            _SpeedPlayer = currVel.magnitude;
        }
        prevPosition = transform.position;

        CircularProgressBar cpb = EnergyLevelBar.GetComponent<CircularProgressBar>();
        cpb.m_FillAmount = _EnergyLevel;

        string EnergyColor = Info2Color;
        if (_EnergyLevel < 0.3f)
        {
            cpb.m_FillColor = UnityEngine.Color.red;
            EnergyColor = DangerColor;
            HelpText.GetComponent<TMPro.TextMeshProUGUI>().text += "Danger: energy low!\n";
            HelpBackground.SetActive(true);
            if (!AlertAudioSouce.isPlaying)
                AlertAudioSouce.Play();
        }

        if (_EnergyLevel > 0.3f && _EnergyLevel < 0.6f)
        {
            EnergyColor = WarningColor;
            cpb.m_FillColor = UnityEngine.Color.yellow;
        }

        if (_EnergyLevel > 0.6f)
            cpb.m_FillColor = UnityEngine.Color.green;

        if (distanceStar < 50)
        {
            HelpText.GetComponent<TMPro.TextMeshProUGUI>().text += "Danger: star too close!\n";
            HelpBackground.SetActive(true);
            if (!AlertAudioSouce.isPlaying)
                AlertAudioSouce.Play();
        }

        if (_EnergyLevel > 0.6f && distanceStar > 50)
            AlertAudioSouce.Stop();

        InfoText.GetComponent<TMPro.TextMeshProUGUI>().text =
            InfoColor + "SPEED: " + _SpeedPlayer.ToString("0.00") + " </color>\n" +
            InfoColor + "ESCAPE: " + (WinHeight - distanceStar).ToString("0.00") + " </color>\n" +
            InfoColor + "STAR: " + distanceStar.ToString("0.00") + " </color>\n" +
            EnergyColor + "ENERGY: " + (_EnergyLevel * 100).ToString("0.00") + " </color>";

        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity))
        {
            if (hit.collider.gameObject != null && hit.collider.gameObject.tag == "CLoop")
            {
                float loopEnergy = (hit.collider.gameObject.transform.localScale.y - CLoopGeneration.CLoopLocalScaleMin) / (CLoopGeneration.CLoopLocalScaleMax - CLoopGeneration.CLoopLocalScaleMin);
                if (hit.collider.gameObject.transform.eulerAngles.y > 180)
                    loopEnergy *= -1;
                Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, (loopEnergy > 0 ? UnityEngine.Color.green : UnityEngine.Color.red));
                LoopText.GetComponent<TMPro.TextMeshProUGUI>().text =
                    "**CORONAL LOOP INFORMATION:\n" +
                    "*" + (loopEnergy > 0 ? InfoColor : WarningColor) + "POWER:" + (loopEnergy > 0 ? "+" : "") + loopEnergy.ToString("0.00") + "</color>\n" +
                    "*" + Info2Color + "DISTANCE: " + hit.distance.ToString("0.00") + "</color>";
            }
        }
        else
        {
            if (InsideLoop == null)
                LoopText.GetComponent<TMPro.TextMeshProUGUI>().text = "**NO LOOP FORWARD";
            else
                LoopText.GetComponent<TMPro.TextMeshProUGUI>().text = "**INSIDE CORONAL LOOP";
        }


        Vector3 direction = transform.position - SunPlane.transform.position;
        Quaternion rotation = transform.rotation;
        float angleWithStar = SignedAngleBetween(transform.forward, direction, transform.forward);
        // no engine inside the loop, everything stop
        if (InsideLoop == null && Input.GetKey(KeyCode.Space) && _EnergyLevel > 0)
        {
            rb.AddForce(transform.forward * SpaceShipSpeed, ForceMode.Force);
            float decrease = DecreaseEnergyStep.x + ((DecreaseEnergyStep.y - DecreaseEnergyStep.x) * (1-System.Math.Abs(angleWithStar % 90) / 90));
            //Debug.Log("Decrease: " + decrease);
            _EnergyLevel -= Time.deltaTime * decrease;
            SetEngineParticle(new Vector3(0, 0, -25));
        }
        else
            SetEngineParticle(new Vector3(0, 0, 0));

        float vertical = Input.GetAxis("Vertical") * VerticalSpeed * Time.deltaTime;
        float horizontal = Input.GetAxis("Horizontal") * HorizontalSpeed * Time.deltaTime;
        if (vertical != 0 && horizontal != 0)
            transform.Rotate(0, vertical * horizontal, 0);
        else
            transform.Rotate(vertical, 0, horizontal * -1);
        
        float appliedFrictionForce = FrictionForce * System.Math.Abs(1 - (rotation.eulerAngles.z % 90)/90);
        _AppliedForce = System.Math.Abs(GravityForce + appliedFrictionForce);
        if (InsideLoop == null)
        {
            float zAngle = rotation.eulerAngles.z > 180 ? rotation.eulerAngles.z - 360 : rotation.eulerAngles.z;
            Vector3 lateralDirection = Camera.main.transform.right * -1;
            if (zAngle < 0)
                lateralDirection = Camera.main.transform.right;
            rb.AddForce(lateralDirection * (System.Math.Abs(rotation.eulerAngles.z % 90)/90) * SpaceShipLateralUnforcedSpeed * Time.deltaTime, ForceMode.Force);
            rb.AddForce(transform.forward * (1 - System.Math.Abs(angleWithStar % 90) / 90) * SpaceShipFowardUnforcedSpeed * Time.deltaTime, ForceMode.Force);
        }

        if (InsideLoop != null && InsideLoop.IsDestroyed())
        {
            Debug.Log("Gone Loop " + InsideLoop.name);
            LoadingAudioSource.Stop();
            InsideLoop = null;
            InsideLoopIndexCorner = -1;
        }

        if (InsideLoop != null)
        {
            float loopEnergy = CLoopGeneration.GetCoronalLoopEnergy(InsideLoop);
            if (HelpText != null)
            {
                HelpText.GetComponent<TMPro.TextMeshProUGUI>().text += "You're inside a coronal magnetic Loop! You can't escape without a burst!\nPoint the prow to the stars and press B!\nRequire half of the energy!";
                HelpBackground.SetActive(true);
            }

            LoopText.GetComponent<TMPro.TextMeshProUGUI>().text =
                    "**CORONAL LOOP INFORMATION:</color>\n" +
                    "*" + (loopEnergy > 0 ? InfoColor : WarningColor) + "POWER:" + (loopEnergy > 0 ? "+" : "") + loopEnergy.ToString("0.00") + "</color>\n" +
                    "*" + InfoColor + "DISTANCE: 0.00</color>";

            if (_EnergyLevel > 0 && _EnergyLevel < 1)
            {
                float newValue = _EnergyLevel + (loopEnergy / InsideLoopDivFactor * Time.deltaTime);
                if (newValue > 1)
                    _EnergyLevel = 1;
                else
                {
                    if (newValue < 0)
                        _EnergyLevel = 0;
                    else
                        _EnergyLevel = newValue;
                }
            }

            if (Input.GetKey(KeyCode.B) && _EnergyLevel > 0)
            {
                // Burst! the only way to escape the star
                rb.AddForce(transform.forward * InsideLoopBurstSpeed * (_EnergyLevel / 2), ForceMode.Impulse);
                SetEngineParticle(new Vector3(0, 0, -25)); //maybe a different color?
                DecreaseEnergy(_EnergyLevel / 2);
                InsideLoop = null;
                InsideLoopIndexCorner = -1;
                LoadingAudioSource.Stop();
                return;
            }

            // no force applied inside a loop
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            GameObject corner = GameObject.Find(InsideLoop.name + "/corner" + InsideLoopIndexCorner);
            //Debug.Log("Position " + corner.transform.position);
            transform.position = Vector3.SmoothDamp(
                            transform.position,
                            corner.transform.position,
                            ref currentVelocity,
                            InsideLoopSmoothTime
                        );
            float distance = Vector3.Distance(transform.position, corner.transform.position);
            //Debug.Log("distance " + distance);
            if (distance <= DeltaDistance)
            {
                InsideLoopIndexCorner++;
                if (InsideLoopIndexCorner >= 8)
                    InsideLoopIndexCorner = 0;
            }
        }

        if (InsideLoop == null)
        {
            Debug.Log("(-GravityForce + appliedFrictionForce)=" + (-GravityForce + appliedFrictionForce));
            rb.AddForce(new Vector3(direction.x > 0 ? 1 : -1, direction.y > 0 ? 1 : -1, direction.z > 0 ? 1 : -1) * (-GravityForce + appliedFrictionForce) * Time.deltaTime, ForceMode.Force);
            // should not go directly into the star but which direction to apply the force?
        }

        
    }

    private float SignedAngleBetween(Vector3 a, Vector3 b, Vector3 n)
    {
        // angle in [0,180]
        float angle = Vector3.Angle(a, b);
        float sign = Mathf.Sign(Vector3.Dot(n, Vector3.Cross(a, b)));

        // angle in [-179,180]
        float signed_angle = angle * sign;

        // angle in [0,360] (not used but included here for completeness)
        //float angle360 =  (signed_angle + 180) % 360;

        return signed_angle;
    }

    private Vector3 GetClosestCorner(out int index)
    {
        float minDistance = float.MaxValue;
        index = -1;
        Vector3 closest = new Vector3(0, 0, 0);
        for (int i = 0; i < 8; i++)
        {
            GameObject corner = GameObject.Find(InsideLoop.name + "/corner" + i);
            float distance = Vector3.Distance(transform.position, corner.transform.position);
            if (distance < minDistance)
            {
                minDistance = distance;
                index = i;
                closest = corner.transform.position;
            }
        }

        return closest;
    }

    private void SetEngineParticle(Vector3 maxEngineFireParticle)
    {
        GameObject[] engineGameObjs = GameObject.FindGameObjectsWithTag("EngineFire");
        foreach (var engineGameObj in engineGameObjs)
        {
            VisualEffect ve = engineGameObj.GetComponent<VisualEffect>();
            if (ve != null)
                ve.SetVector3("MaxParticleSpeed", maxEngineFireParticle);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "WinPosition")
            SceneManager.LoadScene("YouWin");

        if (gameover)
            return;

        if (collision.gameObject.tag == "CLoop")
        {
            VisualEffect ve = collision.gameObject.GetComponentInChildren<VisualEffect>();
            ve.SetFloat("ActiveColorAlpha", 0.03f);
            InsideLoop = collision.gameObject;
            GetClosestCorner(out InsideLoopIndexCorner);
            Debug.Log("Inside Loop " + collision.gameObject.name);
            LoadingAudioSource.Play();
        }
        if (collision.gameObject.tag == "Fireball")
        {
            Debug.Log("Fireball OnCollisionEnter: " + collision.gameObject.name);
            GameObject obj = Instantiate(Explosion, collision.transform.position, Quaternion.identity);
            obj.name = "Explosion";
            Destroy(collision.gameObject);
            Destroy(obj, DestroyTime);
            GameObject smokeobj = Instantiate(Smoke, collision.transform.position, Quaternion.identity, gameObject.transform);
            smokeobj.name = "Smoke";
            smokeobj.tag = "Smoke";
            DecreaseEnergy(collision.gameObject.GetComponent<Rigidbody>().mass / FireBallDecreaseEnergy);
            ExplosionAudioSource.Play();
        }
        if (collision.gameObject.tag == "SunPlane")
        {
            ExplosionAudioSource.Play();
            GameObject Spaceship = GameObject.FindGameObjectWithTag("Spaceship");
            Spaceship.SetActive(false);
            GameObject EngineFire = GameObject.FindGameObjectWithTag("EngineFire");
            EngineFire.SetActive(false);
            GameObject[] smokeObjs = GameObject.FindGameObjectsWithTag("Smoke");
            foreach (GameObject gameObject in smokeObjs)
                Destroy(gameObject);
            SpaceshipExplosion.SetActive(true);
            gameover = true;
            GameOverObject.SetActive(true);
        }
    }

    private void DecreaseEnergy(float lostEnergy)
    {
        _EnergyLevel -= lostEnergy;
        if (_EnergyLevel < 0)
            _EnergyLevel = 0;
    }
}
