using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.VFX;

public class FBallGen : MonoBehaviour
{
    private System.Random rand = new System.Random();

    public GameObject FireBall;

    public Camera VisibilityCamera;

    // borders are colliders
    public GameObject MinX, MaxX, MinZ, MaxZ, MinY, MaxY;

    public int SimultaneousObjects;

    public int MaxParticle;

    private List<GameObject> lGameObject;

    public float DestroyTime = 15;


    // Use this for initialization
    void Start()
    {
        lGameObject = new List<GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
        if (FireBall == null || MinX == null || MaxX == null || MinZ == null || MaxZ == null || MinY == null || MaxY == null)
            return;

        if (this.GetNotDestroyedObjList() >= MaxParticle)
            return;

        for (int i = 0; i < SimultaneousObjects; i++)
            CreateObject();
    }

    private int GetNotDestroyedObjList()
    {
        int res = 0;
        foreach (GameObject go in lGameObject)
            if (!go.IsDestroyed())
                res++;
        return res;
    }

    private void CreateObject()
    {
        GameObject obj;
        Vector3 position = GetVisiblePosition(); // new Vector3(Random.Range(MinX.transform.position.x, MaxX.transform.position.x), Random.Range(MinY.transform.position.x, MaxY.transform.position.x), Random.Range(MinZ.transform.position.z, MaxZ.transform.position.z));
        obj = Instantiate(FireBall, position, Quaternion.identity);
        obj.name = "Fireball" + rand.Next();
        Destroy(obj, DestroyTime);
        lGameObject.Add(obj);

    }

    private Vector3 GetVisiblePosition()
    {
        Vector3 position = new Vector3(
            Random.Range(MinX.transform.position.x, MaxX.transform.position.x),
            Random.Range(MinY.transform.position.y, MaxY.transform.position.y),
            Random.Range(MinZ.transform.position.z, MaxZ.transform.position.z)
        );
        Camera toUse = Camera.main;
        if (VisibilityCamera != null)
        {
            toUse = VisibilityCamera;
        }
        Vector3 viewPos = toUse.WorldToViewportPoint(position);
        if (viewPos.x >= 0 && viewPos.x <= 1 && viewPos.y >= 0 && viewPos.y <= 1 && viewPos.z > 0)
        {
            return position;
        }
        return this.GetVisiblePosition();
    }   
}
