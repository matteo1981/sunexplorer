using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyWhenNotVisible : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        Vector3 viewPos = Camera.main.WorldToViewportPoint(this.gameObject.transform.position);
        if (!(viewPos.x >= 0 && viewPos.x <= 1 && viewPos.y >= 0 && viewPos.y <= 1 && viewPos.z > 0))
        {
            // not visible
            Destroy(this.gameObject, 0.1f);
        }
    }
}
