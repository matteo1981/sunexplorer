using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class TargetIndicator : MonoBehaviour
{
    public GameObject Arrow;
    public float RotationSpeed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        GameObject target = null;
        float maxEnergy = float.MinValue;
        GameObject[] objs = GameObject.FindGameObjectsWithTag("CLoop");
        foreach (GameObject obj in objs)
        {
            float energy = CLoopGeneration.GetCoronalLoopEnergy(obj);
            Vector3 viewPos = Camera.main.WorldToViewportPoint(obj.transform.position);
            if (viewPos.x >= 0 && viewPos.x <= 1 && viewPos.y >= 0 && viewPos.y <= 1 && viewPos.z > 0)
            {
                if (energy > 0)
                {
                    Arrow.SetActive(false);
                    return;
                }
                continue;
            }

            if (energy > maxEnergy)
            {
                maxEnergy = energy;
                target = obj;
            }
        }
        if (target == null)
        {
            Arrow.SetActive(false);
            return;
        }

        Arrow.transform.rotation = Quaternion.Slerp(Arrow.transform.rotation, Quaternion.LookRotation(target.transform.position - Arrow.transform.position),
            RotationSpeed * Time.deltaTime);
        Arrow.SetActive(true);

    }
}
