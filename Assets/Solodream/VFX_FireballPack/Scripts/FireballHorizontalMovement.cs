using UnityEngine;
using UnityEngine.VFX;

namespace FireballMovement
{    public class FireballHorizontalMovement : MonoBehaviour
    {
        [SerializeField] private Vector3 _startingPos;
        [SerializeField] private float delay = 6f;
        [SerializeField] private float timer = 0f;

        public Vector3 MinRelativeForce;
        public Vector3 MaxRelativeForce;

        private Rigidbody _fireball;
        private VisualEffect _fireballTrails;

        private const string _fireballTrailsActiveString = "FireballTrailsActive";

        void Start()
        {           
            _fireball = gameObject.GetComponent<Rigidbody>();
            _fireballTrails = gameObject.GetComponent<VisualEffect>();
            _startingPos = _fireball.transform.localPosition;
        }

        void Update()
        {
            if (timer < delay)
            {
                timer += Time.deltaTime;
                _fireballTrails.enabled = true;
                // MDC add certain force
                _fireball.AddRelativeForce(new Vector3(Random.Range(MinRelativeForce.x, MaxRelativeForce.x), Random.Range(MinRelativeForce.y, MaxRelativeForce.y), Random.Range(MinRelativeForce.z, MaxRelativeForce.z)));
            }
            else if (timer >= delay)
            {
                ResetAll();
            }

            if (_fireball.velocity.x > 1 || _fireball.velocity.x < -1)
            {
                _fireballTrails.SetBool(_fireballTrailsActiveString, true);
            }
            else
            {
                _fireballTrails.SetBool(_fireballTrailsActiveString, false);
            }
        }

        private void ResetAll()
        {
            _fireballTrails.enabled = false;
            _fireball.velocity = Vector3.zero;
            _fireball.transform.localPosition = _startingPos;
            timer = 0;
        }
    }
}