# SunEscape

This is a unity 3d project for a game with educational purposes. 

The project aims at introducing young generations to the physics of stellar interiors. 

We initially focused on the Sun, the best known star. In our game, real physical processes, such as solar flares and coronal loops, are simulated. Besides stellar physics, we want to trigger the curiosity of young people on fundamental physics, by letting our starship interact with magnetic fields. The latter are present everywhere in the Universe, from galaxies to planets (Earth included): therefore, each learning experiment (included games) able to increase the interest on these phenomena has to be pursued.

## Project details

The project was born with the idea to make users to better understand the physics of the Sun. In particular the starting point was the sun coronal loops. 

In order to develop them, the visual effect graph feature of unity3d was used together with a shader. Same features has been used for the flares and other collisions. 

The spaceship is under the influnce of many forces: the gravity, its engine, the force of the coronal loops and other forces like friction of the ship. 

The project has 4 main scenes: the help/introduction, the settings, the win and main scene. All of them are stored in the scene folder. 


## Why a video game for educational purposes

The game, in general, is an activity to which a human being dedicates himself with no other purpose than recreation and leisure. Through gratification, many abilities are developed that can be both physical and intellectual. By definition something is difficult that can only be made easy through learning and perseverance to the point of personal or group gratification. For these reasons, it is a powerful teaching tool, even if it is not always possible to create a game to explain complex concepts.

Video games (all of them) lead the player to the knowledge of a virtual reality and, very often, to the correlation between virtual and real reality. They always require active participation because they force you to decide, choose, assign priorities and therefore learn to make the right decisions. Within the game there are always problems to solve or obstacles to overcome and this develops problem solving and multi-tasking skills, clearly useful in everyday life.

With these premises, INAF-OAAb presents the video game "SunEscape", which leads players to INTERACT with our star, the Sun, to introduce the younger generations to the discovery of physics inside stars.
In this game, real physical processes such as flares and coronal loops are simulated and represent obstacles, but also opportunities. The aim of the game is not to fall to the surface (due to the force of gravity) and escape towards Earth, crossing magnetic fields that recharge the engine of a starship.

### Italian

Il gioco, in generale, è un'attività a cui un essere umano si dedica senza altro fine se non la ricreazione e lo svago. Attraverso la gratificazione vengono sviluppate molte abilità che possono essere sia fisiche sia intellettive. E' per definizione qualcosa di difficile che solo attraverso l'apprendimento e la costanza si può rendere rendere facile fino alla gratificazione personale o di gruppo. Per queste motivazioni è un potente strumento didatticO, anche se non sempre è possibile realizzare un gioco per spiegare concetti complessi. 

I video giochi (tutti) portano il giocatore alla conoscenza di una realtà virtuale e, molto spesso, alla correlazione tra realtà virtuale e quella reale. Richiedono sempre una partecipazione attiva perché obbligano a decidere, scegliere, assegnare priorità e quindi imparare a prendere le decisioni giuste. All'interno del gioco esistono sempre problemi da risolvere o ostacoli da superare e questo sviluppa abilità di problem solving e multi tasking, chiaramente utile nella vita di tutti i giorni. 

Con queste premesse l'INAF-OAAb presenta il video gioco "SunEscape", che porta i giocatori AD INTERAGIRE con la nostra stella, il Sole, per introdurre le giovani generazioni alla scoperta della fisica all'interno delle stelle. 
In questo gioco processi fisici reali come flares e loop coronali sono simulati e rappresentano ostacoli, ma anche opportunità. Scopo del gioco è non cadere sulla superficie (a causa della forza di gravità) e scappare verso la Terra, attraversando campi magnetici che ricaricano il motore di una navicella stellare. 

